# Jupyter

オフィシャルイメージのPythonをベースとしたJupyter, 最小限の構成で,
必要なパッケージがあれば, これをベースイメージとして別途インストールする想定です.

## Description

このイメージには, 次の特徴があります.

Jupyterは一般ユーザ (ユーザ名 jupyter) のセッションで実行しています.
rootで実行すると, Jupyterのターミナルから破壊的な変更ができてしまうためです.

Pythonのオフィシャルイメージをベースにして,
Jupyterとその依存パッケージのみをインストールしています.


## Usage

Jupyterとその依存パッケージだけがあればいいのであれば, イメージをそのまま使うことができます.
下記のコマンドを実行し, `http://container-ip:8888` へブラウザからアクセスすると, 
Jupyterの画面がでてきます.

```bash
# docker run kuchida1981/jupyter:latest
```

`:latest` でPython3+Jupyterです. `kuchida1981/jupyter:2` とすると, Python2ベースを利用できます.

ポートフォワードしたほうが使いやすいことが多いと思います.

```bash
# docker run -p 8888:8888 kuchida1981/jupyter:latest
```

このコマンドでコンテナを立ち上げた場合は, `http://127.0.0.1:8888` で
アクセスできるようになります.


### パッケージを追加する方法

Jupyter以外に必要なモジュールがあることは多いと思います.

`# docker exec -it container-id bash` でコンテナにログインして,
`apt-get` コマンドや `pip` コマンドで使いたいパッケージ/モジュールを追加することができます.

もしくはこのイメージをベースイメージとして, モジュール追加することもできます.

matplotlibを利用可能にしたい場合は, 次のようなDockerfileを作ってビルドしてください.

```
FROM kuchida1981/jupyter:latest
USER root
RUN apt-get update && apt-get install -y pkg-config libfreetype6-dev
RUN pip install matplotlib
```

### 他のコンテナと連動して使う

他のコンテナと連動して使いたい場合には, Docker Composeの仕組みを使うのが簡単です.

データ解析のためのデータベース mariadbとphpmyadminと一緒に使いたい場合には,
たとえば次のような内容の `docker-compose.yml` を用意してやることが考えられます.
```yaml
version: '2'
services:
        database:
                image: mariadb
                environment:
                        MYSQL_ROOT_PASSWORD: secret
                        MYSQL_DATABASE: jupyter
                        MYSQL_USER: jupyter
                        MYSQL_PASSWORD: secret
                ports:
                        - "23306:3306"
                volumes:
                        - ./mysql:/var/lib/mysql
        database_admin:
                image: phpmyadmin/phpmyadmin
                links:
                        - database:db
                ports:
                        - "20081:80"
        jupyter_server:
                image: kuchida1981/jupyter:3
                depends_on:
                        - database
                ports:
                        - "28888:8888"
                volumes:
                        - .:/home/jupyter
```

## Licence

MIT


## Author

Kosuke Uchida
